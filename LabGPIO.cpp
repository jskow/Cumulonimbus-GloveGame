/*
 * LabGPIO.cpp
 *
 *  Created on: Feb 15, 2018
 *      Author: jskow
 */
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <LPC17xx.h>
#include "LabGPIO.hpp"
#include "uart0_min.h"

//Create GPIO 0 instance by specifying pin number
LabGPIO::LabGPIO(uint8_t pin_no, uint8_t port_num)
{
	pin = pin_no;
	port_no = port_num;
}

//Destroy instance
LabGPIO::~LabGPIO()
{

}

/**
     * Should alter the hardware registers to set the pin as an input
 */
void LabGPIO::setAsInput()
{
	//LPC_GPIO0->FIODIR &= ~(1 << pin);
	ports[port_no]->FIODIR &= ~(1 << pin);
}

/**
 * Should alter the hardware registers to set the pin as an output
 */
void LabGPIO::setAsOutput()
{
	ports[port_no]->FIODIR |= (1 << pin);
}

/**
 * Should alter the set the direction output or input depending on the input.
 *
 * @param {bool} output - true => output, false => set pin to input
 */
void LabGPIO::setDirection(bool output)
{
	if (output == true)
	{
		this->setAsOutput();
	} else
	{
		this->setAsInput();
	}
}

/**
 * Should alter the hardware registers to set the pin as high
 */
void LabGPIO::setHigh()
{
	ports[port_no]->FIOSET |= (1 << pin);
}

/**
 * Should alter the hardware registers to set the pin as low
 */
void LabGPIO::setLow()
{
	ports[port_no]->FIOCLR |= (1 << pin);
}

/**
 * Should alter the hardware registers to set the pin as low
 *
 * @param {bool} high - true => pin high, false => pin low
 */
void LabGPIO::set(bool high)
{
	if (high == true)
	{
		this->setHigh();
	} else {
		this->setLow();
	}
}

void LabGPIO::toggle()
{
	if (this->getLevel() == true) {
		this->setLow();
	} else {
		this->setHigh();
	}
}

/**
 * Should return the state of the pin (input or output, doesn't matter)
 *
 * @return {bool} level of pin high => true, low => false
 */
bool LabGPIO::getLevel()
{
	return ((ports[port_no]->FIOPIN) & (1 << pin));
}
