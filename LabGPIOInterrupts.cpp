/*
 * LabGPIOInterrupts.cpp
 *
 *  Created on: Feb 18, 2018
 *      Author: jskow
 *  Singleton assistance from https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
 */
#include "LabGPIOInterrupts.hpp"
#include "LPC17xx.h"
#include "lpc_isr.h"
#include "uart0_min.h"
#include "stdio.h"

//GPIO ISR lookup table
//As user implements their own ISR, replace these pointers
static void (* gpio_isr_tbl[NUM_GPIO_PORTS][PINS_PER_PORT])(void) = {
	0
};
int get_bit_set(uint32_t &bit_field);

//Get singleton instance pointer
LabGPIOInterrupts * LabGPIOInterrupts::getInstance()
{
	//This will only instantiate instance on first call
	static LabGPIOInterrupts instance;
	return &instance;
}

/**
 * 1) Should setup register "externalIRQHandler" as the EINT3 ISR.
 * 2) Should configure NVIC to notice EINT3 IRQs.
 */
void LabGPIOInterrupts::init()
{
	isr_register(EINT3_IRQn, this->externalIRQHandler);
	NVIC_EnableIRQ(EINT3_IRQn);
}

/**
 * This handler should place a function pointer within the lookup table for the externalIRQHandler to find.
 *
 * @param[in] port         specify the GPIO port
 * @param[in] pin          specify the GPIO pin to assign an ISR to
 * @param[in] pin_isr      function to run when the interrupt event occurs
 * @param[in] condition    condition for the interrupt to occur on. RISING, FALLING or BOTH edges.
 * @return should return true if valid ports, pins, isrs were supplied and pin isr insertion was sucessful
 */
bool LabGPIOInterrupts::attachInterruptHandler(uint8_t port, uint32_t pin, void (*pin_isr)(void), InterruptCondition_E condition)
{
	//Enable interrupt for edge
	if (port == 0)
	{
		switch (condition) {
			case RISING_IRQ:
				LPC_GPIOINT->IO0IntEnR |= (1 << pin);
				break;
			case FALLING_IRQ:
				LPC_GPIOINT->IO0IntEnF |= (1 << pin);
				break;
			case BOTH_IRQ:
				LPC_GPIOINT->IO0IntEnF |= (1 << pin);
				LPC_GPIOINT->IO0IntEnR |= (1 << pin);
				break;
			default:
				break;
		}
	} else if (port == 1){
		switch (condition) {
			case RISING_IRQ:
				LPC_GPIOINT->IO2IntEnR |= (1 << pin);
				break;
			case FALLING_IRQ:
				LPC_GPIOINT->IO2IntEnF |= (1 << pin);
				break;
			case BOTH_IRQ:
				LPC_GPIOINT->IO2IntEnF |= (1 << pin);
				LPC_GPIOINT->IO2IntEnR |= (1 << pin);
				break;
			default:
				break;
		}
	}
	gpio_isr_tbl[port][pin] = pin_isr;
}

/**
 * After the init function has run, this will be executed whenever a proper
 * EINT3 external GPIO interrupt occurs. This function figure out which pin
 * has been interrupted and run the ccorrespondingISR for it using the lookup table.
 *
 * VERY IMPORTANT! Be sure to clear the interrupt flag that caused this
 * interrupt, or this function will be called again and again and again, ad infinitum.
 *
 * Also, NOTE that your code needs to be able to handle two GPIO interrupts occurring
 * at the same time.
 */
void LabGPIOInterrupts::externalIRQHandler(void)
{
	//This ISR can handle up to TWO pins at once
	uint8_t num_irq = 0, pin_num = 0;
	uint32_t io0_irqs_r, io0_irqs_f, io2_irqs_r, io2_irqs_f, int_status;

	//Read all the interrupt status
	io0_irqs_r = LPC_GPIOINT->IO0IntStatR;
	io0_irqs_f = LPC_GPIOINT->IO0IntStatF;
	io2_irqs_r = LPC_GPIOINT->IO2IntStatR;
	io2_irqs_f = LPC_GPIOINT->IO2IntStatF;
	int_status = LPC_GPIOINT->IntStatus;

	while ((num_irq < 2 && int_status != 0))
	{
		if (io0_irqs_r != 0)
		{
			//Get pin where interrupt occurred
			pin_num = get_bit_set(io0_irqs_r);
			//Handle up to two IRQ
			num_irq++;
			//Call ISR
			gpio_isr_tbl[0][pin_num]();
			//Clear the interrupt
			LPC_GPIOINT->IO0IntClr = (1 << pin_num);
		}

		if (io0_irqs_f != 0)
		{
			//Get pin where interrupt occurred
			pin_num = get_bit_set(io0_irqs_f);
			//Handle up to two IRQ
			num_irq++;
			//Call ISR
			gpio_isr_tbl[0][pin_num]();
			//Clear the interrupt
			LPC_GPIOINT->IO0IntClr = (1 << pin_num);
		}

		if (io2_irqs_r != 0)
		{
			//Get pin where interrupt occurred
			pin_num = get_bit_set(io2_irqs_r);
			//Handle up to two IRQ
			num_irq++;
			//Call ISR
			gpio_isr_tbl[1][pin_num]();
			//Clear the interrupt
			LPC_GPIOINT->IO2IntClr = (1 << pin_num);
		}

		if (io2_irqs_f != 0)
		{
			//Get pin where interrupt occurred
			pin_num = get_bit_set(io2_irqs_f);
			//Handle up to two IRQ
			num_irq++;
			//Call ISR
			gpio_isr_tbl[1][pin_num]();
			//Clear the interrupt
			LPC_GPIOINT->IO2IntClr = (1 << pin_num);
		}
		int_status = LPC_GPIOINT->IntStatus;
		num_irq++;
	}
}


//Loop through the bit field, if you find a high bit clear it.
//Return the position of the bit you cleared.
int get_bit_set(uint32_t &bit_field)
{
	uint32_t bit;
	for (bit=0;bit<32;bit++)
	{
		if (bit_field & (1 << bit))
		{
			bit_field &= ~(1 << bit);
			break;
		}
	}
	return bit;
}
