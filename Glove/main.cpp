/*
SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief This is the application entry point.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "tasks.hpp"
#include "uart0_min.h"
#include "uart2.hpp"
#include "LPC17xx.h"
#include "labgpio.hpp"
#include "stdio.h"
#include "utilities.h"
#include "printf_lib.h"
#include "adc_driver.hpp"
#include "uart2.hpp"
#include "io.hpp"
#include "storage.hpp"
#include "event_groups.h"
#include "examples/examples.hpp"
#include "time.h"
#include "string.h"
#include "adc0.h"
#include "queue.h"
//#include "labadc.hpp"

//#define STACK_SIZE 1024
//
//void  task(void * p)
//{
//    char ch[2] = {0};
//    Uart2 &u = Uart2::getInstance();
//    while(1)
//    {
//        if(u.getChar(ch, portMAX_DELAY))
//        {
//            ch[1] = '\0';
//            if (ch[0] == 'R')
//                printf("rock recieved\n");
//            else if (ch[0] == 'P')
//                printf("paper recieved\n");
//            else if (ch[0] == 'S')
//                printf("scissors recieved");
//            u.putline("A",1000);
//        }
//        vTaskDelay(100);
//    }
//}
//
//int main (void)
//{
//    Uart2 &u = Uart2::getInstance();
//    u.init(9600);
//    xTaskCreate(task, "task", STACK_SIZE, (void *)'a', 1, NULL);
//    vTaskStartScheduler();
//    return 0;
//}

#define MASTER 1

//project

 #define STONE 150
 #define PAPER

Uart2 &uartObj = Uart2::getInstance();
ADCDriver adcObj;
QueueHandle_t q;

void activateVibr()
{
    LPC_GPIO0->FIOSET = (1<<0);
}

void clearVibr()
{
    LPC_GPIO0->FIOCLR = (1<<0);
}

void adcTask(void* p)
{
    char status[10] = {0};
    uint16_t flex = 0;
    uint16_t flexKey = 0;
    while(1)
    {
            flex = adcObj.readADCVoltageByChannel(3);
            flexKey = adcObj.readADCVoltageByChannel(4);

            printf("flex sensor:%d\n",flex);
            printf("flexKey sensor:%d\n",flexKey);

            if(flexKey < 175 && flex < 260)
            {
                strncpy(status, "R", 1);
                xQueueSend(q, status, 0);
            }
            else if(flexKey > 175 && flex > 260)
            {
                strncpy(status, "P", 1);
                xQueueSend(q, status, 0);
            }
            else if(flexKey < 175 && flex > 260)
            {
                strncpy(status, "S", 1);
                xQueueSend(q, status, 0);
            }
            vTaskDelay(1000);
    }
}

void btTransferTask(void* p)
{
    char buffer[10] = {0};
    while(1)
    {
        if (xQueueReceive(q, &buffer, portMAX_DELAY))
        {
            //send over bluetooth to game

            buffer[9] = '\0';
            printf("Buffer %s\n", buffer);
            uartObj.putline(buffer);
        }
        vTaskDelay(1000);
    }
}

void btRecieveTask(void* p)
{
    while(1)
    {
        char buff[3] = {0};
        if(uartObj.getChar(buff,0))
        {
            buff[2] = '\0';
            printf("receiving\n");
            // if feedback opcode
            if(buff[0] == 'A')
            {
                //vibration sensors
                printf("vibrating\n");
                activateVibr();
                delay_ms(1000);
                clearVibr();
            }
             printf("%s",buff);
        }
        vTaskDelay(1000);
    }
}

#if 0
int main()
{
    q = xQueueCreate(1, 10);
    uartObj.init(9600);

    adcObj.adcInitBurstMode();
    adcObj.adcSelectPin(ADCDriver::ADC_PIN_0_26);
    adcObj.adcSelectPin(ADCDriver::ADC_PIN_1_30);

    LPC_GPIO0->FIODIR |= (1<<0);
    LPC_GPIO0->FIODIR |= (1<<1);
    LPC_GPIO2->FIODIR |= (1<<6);
    LPC_GPIO2->FIODIR |= (1<<7);

    LPC_GPIO0->FIOSET = (1<<1);
    LPC_GPIO2->FIOSET = (1<<6);
    LPC_GPIO2->FIOCLR = (1<<7);


    //while(1);

    xTaskCreate(btTransferTask, "btTransfer", 1024, 0, PRIORITY_MEDIUM, 0);
    xTaskCreate(btRecieveTask, "btRecieve", 1024, 0, PRIORITY_MEDIUM, 0);
    xTaskCreate(adcTask, "Flex", 1024, 0, PRIORITY_LOW, 0);

    vTaskStartScheduler();

    return 0;

}
#endif
#if 0
/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief This is the application entry point.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "tasks.hpp"
#include "uart0_min.h"
#include "uart2.hpp"
#include "LPC17xx.h"
#include "labgpio.hpp"
#include "stdio.h"
#include "utilities.h"
#include "printf_lib.h"
#include "adc_driver.hpp"
#include "uart_drv.hpp"
#include "io.hpp"
#include "storage.hpp"
#include "event_groups.h"
#include "examples/examples.hpp"
#include "time.h"
#include "string.h"
#include "adc0.h"
#include "queue.h"
//#include "labadc.hpp"

//#define STACK_SIZE 1024
//
//void  task(void * p)
//{
//    char ch[2] = {0};
//    Uart2 &u = Uart2::getInstance();
//    while(1)
//    {
//        if(u.getChar(ch, portMAX_DELAY))
//        {
//            ch[1] = '\0';
//            if (ch[0] == 'R')
//                printf("rock recieved\n");
//            else if (ch[0] == 'P')
//                printf("paper recieved\n");
//            else if (ch[0] == 'S')
//                printf("scissors recieved");
//            u.putline("A",1000);
//        }
//        vTaskDelay(100);
//    }
//}
//
//int main (void)
//{
//    Uart2 &u = Uart2::getInstance();
//    u.init(9600);
//    xTaskCreate(task, "task", STACK_SIZE, (void *)'a', 1, NULL);
//    vTaskStartScheduler();
//    return 0;
//}

#define MASTER 1

//project

 #define STONE 150
 #define PAPER

Uart2 &uartObj = Uart2::getInstance();
ADCDriver adcObj;
QueueHandle_t q;

void activateVibr()
{
    LPC_GPIO0->FIOSET = (1<<0);
}

void clearVibr()
{
    LPC_GPIO0->FIOCLR = (1<<0);
}

void adcTask(void* p)
{
    char status[10] = {0};
    uint16_t flex = 0;
    uint16_t flexKey = 0;
    while(1)
    {
            flex = adcObj.readADCVoltageByChannel(3);
            flexKey = adcObj.readADCVoltageByChannel(4);

            printf("flex sensor:%d\n",flex);
            printf("flexKey sensor:%d\n",flexKey);

            if(flexKey < 175 && flex < 260)
            {
                strncpy(status, "R", 1);
                xQueueSend(q, status, 0);
            }
            else if(flexKey > 175 && flex > 260)
            {
                strncpy(status, "P", 1);
                xQueueSend(q, status, 0);
            }
            else if(flexKey < 175 && flex > 260)
            {
                strncpy(status, "S", 1);
                xQueueSend(q, status, 0);
            }
            vTaskDelay(1000);
    }
}

void btTransferTask(void* p)
{
    char buffer[10] = {0};
    while(1)
    {
        if (xQueueReceive(q, &buffer, portMAX_DELAY))
        {
            //send over bluetooth to game

            buffer[9] = '\0';
            printf("Buffer %s\n", buffer);
            uartObj.putline(buffer);
        }
        vTaskDelay(1000);
    }
}

void btRecieveTask(void* p)
{
    while(1)
    {
        char buff[3] = {0};
        if(uartObj.getChar(buff,0))
        {
            buff[2] = '\0';
            printf("receiving\n");
            // if feedback opcode
            if(buff[0] == 'A')
            {
                //vibration sensors
                printf("vibrating\n");
                activateVibr();
                delay_ms(1000);
                clearVibr();
            }
             printf("%s",buff);
        }
        vTaskDelay(1000);
    }
}

#if 0
int main()
{
    q = xQueueCreate(1, 10);
    uartObj.init(9600);

    adcObj.adcInitBurstMode();
    adcObj.adcSelectPin(ADCDriver::ADC_PIN_0_26);
    adcObj.adcSelectPin(ADCDriver::ADC_PIN_1_30);

    LPC_GPIO0->FIODIR |= (1<<0);
    LPC_GPIO0->FIODIR |= (1<<1);
    LPC_GPIO2->FIODIR |= (1<<6);
    LPC_GPIO2->FIODIR |= (1<<7);

    LPC_GPIO0->FIOSET = (1<<1);
    LPC_GPIO2->FIOSET = (1<<6);
    LPC_GPIO2->FIOCLR = (1<<7);


    //while(1);

    xTaskCreate(btTransferTask, "btTransfer", 1024, 0, PRIORITY_MEDIUM, 0);
    xTaskCreate(btRecieveTask, "btRecieve", 1024, 0, PRIORITY_MEDIUM, 0);
    xTaskCreate(adcTask, "Flex", 1024, 0, PRIORITY_LOW, 0);

    vTaskStartScheduler();

    return 0;
}
#endif
#endif
