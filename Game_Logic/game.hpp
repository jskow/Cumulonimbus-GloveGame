/*
 * game.hpp
 *
 * This header defines the Game class.
 * Each game type will inherit this class.
 *  Created on: Apr 28, 2018
 *      Author: jskow
 */

#ifndef L5_APPLICATION_PROJECT_LOGIC_GAME_HPP_
#define L5_APPLICATION_PROJECT_LOGIC_GAME_HPP_

#include "display.hpp"
#include <stdint.h>
#include "player.hpp"
//#include "controller.hpp"
#include "../Display/LEDMatrixGPIO.hpp"

void run_game(void *in_game);

class Game {

private:
	//Define up to 255 game modes
	uint8_t mode;
	bool start;
	bool stop;
	bool in_progress;
	//Stages of your game
	uint32_t stage;
	//Players in the game
	//Our games only support 2 players
	Player *player_one_inst;
	Player *player_two_inst;
	//LEDMatrixGPIO *display_inst;

	//Game(){};
    //friend class SingletonTemplate<Game>;

//    Game();
  //  virtual ~Game(){};

public:
	typedef enum{
		PLAYER_1,
		PLAYER_2,
	} player_num_t;


	//These provide logic to the game
	bool start_interrupt;
	bool stop_interrupt;
	bool reset_interrupt;
	bool mode_interrupt;

	Game(){start_interrupt = 0; mode_interrupt = 0;};
	virtual ~Game(){};
	void init();


	Player *player_one(){return player_one_inst;};
	Player *player_two(){return player_two_inst;};
	//LEDMatrixGPIO *display(){return display_inst;};

	//Advance game to the next stage
	void next_stage() {this->stage++;}
	//Advance to next mode
	void next_mode() {this->mode++;}

	//Reset mode/stage
	void reset_stage(){this->stage = 0;}
	void reset_mode(){this->mode = 0;}

	//Get current mode, stage
	uint8_t get_mode(){return this->mode;}
	uint32_t get_stage(){return this->stage;}
	bool get_stop(){return this->stop;}
	bool get_start(){return this->start;}

	//Reset game state/mode/stage
	void reset_game() {this->start = 0; this->stop = 1; this->stage = 0; this->mode=0; this->in_progress=0;}

	//Start the game from either the beginning, or resume from a pause
	void start_game();

	//Stop game is pausing the current stage, or can end the current round
	void stop_game()	{this->start = 0; this->stop = 1; this->in_progress=0;}

	//Stage handling function.  Each sub class needs to define this function
	virtual void call_stage(uint32_t to_stage){};

	//Attach a player to the game
	//Controller will be a part of the player
	void add_player(Player *player_inst, Game::player_num_t player_num);

	//Attach display to the game
	//void add_display(LEDMatrixGPIO *display_in){this->display_inst = display_in;};

	//Set interrupt values
	void toggle_start_intr(){this->start_interrupt = !this->start_interrupt;};
	void toggle_stop_intr(){this->stop_interrupt = !this->stop_interrupt;};
	void toggle_reset_intr(){this->reset_interrupt = !this->reset_interrupt;};
	void toggle_mode_intr(){this->mode_interrupt = !this->mode_interrupt;};

	//Set interrupt values
	void set_start_intr(){this->start_interrupt = 1;};
	void set_stop_intr(){this->stop_interrupt = 1;};
	void set_reset_intr(){this->reset_interrupt = 1;};
	void set_mode_intr(){this->mode_interrupt = 1;};

	//Clear interrupt values
	void clear_start_intr(){this->start_interrupt = 0;};
	void clear_stop_intr(){this->stop_interrupt = 0;};
	void clear_reset_intr(){this->reset_interrupt = 0;};
	void clear_mode_intr(){this->mode_interrupt = 0;};

	//Get interrupt values
	bool get_start_intr(){return this->start_interrupt;};
	bool get_stop_intr(){return this->stop_interrupt;};
	bool get_reset_intr(){return this->reset_interrupt;};
	bool get_mode_intr(){return this->mode_interrupt;};


	//This is the game engine.  It calls the next stage, handles inputs, etc.
	friend void run_game(void *in_game);
};


#endif /* L5_APPLICATION_PROJECT_LOGIC_GAME_HPP_ */
