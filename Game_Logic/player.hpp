/*
 * player.hpp
 *
 *  Created on: Apr 28, 2018
 *      Author: jskow
 */

#ifndef L5_APPLICATION_PROJECT_LOGIC_PLAYER_HPP_
#define L5_APPLICATION_PROJECT_LOGIC_PLAYER_HPP_

#include "controller.hpp"

class Player {

private:
	Controller *controller_inst;
	uint32_t score;

public:
	Player(){this->score=0;};
	Player(Controller *contr_in){controller_inst = contr_in;this->score=0;};
	~Player(){};

	//Attach controller to the player
	void attach_contr(Controller *contr_in){this->controller_inst = contr_in;}

	//Get/increment/reset score
	uint32_t get_score(){return this->score;}
	void incr_score(){this->score++;}
	void reset_score(){this->score=0;}

	//Get controller input
	uint8_t get_contr_input(){return this->controller_inst->get_input();};

	//Send controller feedback
	void send_feedback(){this->controller_inst->send_feedback();};
};


#endif /* L5_APPLICATION_PROJECT_LOGIC_PLAYER_HPP_ */
