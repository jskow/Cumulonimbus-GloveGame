/*
 * rock_paper_scissors.hpp
 *
 *  Created on: Apr 28, 2018
 *      Author: jskow
 */

#ifndef L5_APPLICATION_PROJECT_LOGIC_ROCK_PAPER_SCISSORS_HPP_
#define L5_APPLICATION_PROJECT_LOGIC_ROCK_PAPER_SCISSORS_HPP_

#include <stdint.h>
#include "game.hpp"
//#include "player.hpp"
#include "singleton_template.hpp"

//class NordicStream : public CharDev, public SingletonTemplate<NordicStream>
class RPS : public SingletonTemplate<RPS>, public Game
{
private:
	uint8_t p1_score;
	uint8_t p2_score;
	uint8_t round_count;
	RPS(){}
    friend class SingletonTemplate<RPS>;

public:
	//User of RPS class should just call start, stop game
	typedef enum{
		ROCK, PAPER, SCISSORS
	} RPS_t;

	//RPS(){this->p1_score = 0; this->p2_score = 0;this->round_count=0;};
	~RPS(){};

	void init() {round_count = 0; p1_score = 0; p2_score = 0;};
	//Call each stage as needed
	void call_stage(uint32_t to_stage);

	//This will be the starting screen
	void stage_0();

	//Round logic
	void stage_1();
	//Show game winner
	void stage_2();
	//Show game over
	void stage_3();

	//Determine if p1 wins or not (0=tie, 1=yes, 2=p2)
	uint8_t p1_won_round(RPS_t p1_choice, RPS_t p2_choice);
};


#endif /* L5_APPLICATION_PROJECT_LOGIC_ROCK_PAPER_SCISSORS_HPP_ */
