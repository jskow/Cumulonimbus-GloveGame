/*
 * rock_paper_scissors.cpp
 *
 *  Created on: Apr 28, 2018
 *      Author: jskow
 */

#include "rock_paper_scissors.hpp"
#include "stdio.h"
#include "math.h"
#include <stdlib.h>
#include <stdio.h>
#include "uart0_min.h"
#include "io.hpp"

extern bool onePlayer;

void RPS::call_stage(uint32_t to_stage)
{
	switch(to_stage){
		case 0:
			this->stage_0();
			break;
		case 1:
			this->stage_1();
			break;
		case 2:
			this->stage_2();
			break;
		case 3:
			this->stage_3();
			break;
		default:
			this->stage_0();
			break;
	}
}

//TODO -> all stages, add display logic
//Entry stage - select mode, etc.
void RPS::stage_0()
{
	if(this->get_stop())
	{
		//If user wants to stop, continue displaying current screen

	} else {
		uart0_puts("stage 0");
		//Draw title screen based on number of players, mode
		if(this->get_mode_intr() == 0)
		{
			onePlayer = true;
			//printf("1p mode\n");
			matrixGPIO.drawStartScreen();
			//1 player mode
		} else {
			//printf("2p mode\n");
			onePlayer = false;
			matrixGPIO.drawStartScreen();
			//2 player mode
		}

		//When user selects start, begin round 1
		//if start_interrupt
		if(this->get_start_intr() == 1)
		{
			this->clear_start_intr();
			this->next_stage();
		}
	}
}

//Round 1!
void RPS::stage_1()
{
	uint8_t p1_in;
	uint8_t p2_in;
	uint8_t result;
	if(this->get_stop())
	{
		//If user wants to stop, continue displaying current screen

	} else {
		//Draw round 1 screen
		switch(round_count)
		{
			//Prepare user to give action
			case	 0:
				matrixGPIO.clearScreen();
				//Prepare user to begin
				round_count++;
				break;
			//Take user action
			case 1:
			default:
				matrixGPIO.drawScoreboard(round_count, this->player_one()->get_score(), this->player_two()->get_score());
				//Get user selection, go to next stage
				p1_in = this->player_one()->get_contr_input();
				if(this->get_mode_intr() != 0)
				{
					p2_in = this->player_two()->get_contr_input();
				} else {
					p2_in = rand()%3;
					//printf("CPU: %d\n", p2_in);
				}
				//Draw results of what players chose
				//printf("p2: %d, p1: %d\n", p2_in, p1_in);
				matrixGPIO.drawAction((LEDMatrixGPIO::Action) (p1_in), (LEDMatrixGPIO::Action) (p2_in));
				result = p1_won_round((RPS_t)p1_in, (RPS_t)p2_in);
				if(result == 1)
				{
					this->player_one()->incr_score();
					matrixGPIO.drawScore(this->player_one()->get_score(), this->player_two()->get_score());
					matrixGPIO.drawRoundEnd(LEDMatrixGPIO::Player1);
				} else if (result == 2){
					this->player_two()->incr_score();
					matrixGPIO.drawScore(this->player_one()->get_score(), this->player_two()->get_score());
					matrixGPIO.drawRoundEnd(LEDMatrixGPIO::Player2);
				} else {
					//If Tie, do this stage again
					matrixGPIO.drawRoundEnd(LEDMatrixGPIO::Neither);
					this->player_one()->send_feedback();
					this->player_two()->send_feedback();
					break;
				}
				//Repeat until 1 player gets a score of 3
				this->player_one()->send_feedback();
				this->player_two()->send_feedback();


				//Play until one player has score of 3
				if((this->player_one()->get_score() == 3) || (this->player_two()->get_score() == 3))
				{
					uart0_puts("game over");
					//matrixGPIO.drawScoreboard(round_count, this->player_one()->get_score(), this->player_two()->get_score());
					this->next_stage();
					break;
				}

				round_count++;
				break;
		}
	}
}

//Game over!
void RPS::stage_2()
{
	if(this->player_one()->get_score() == 3)
	{
		matrixGPIO.drawGameOver(LEDMatrixGPIO::Player1);
	} else {
		matrixGPIO.drawGameOver(LEDMatrixGPIO::Player2);
	}
	this->next_stage();
}

//Show game over
void RPS::stage_3()
{
	 //matrixGPIO.drawGameOver();

	this->player_one()->reset_score();
	this->player_two()->reset_score();
	round_count = 0;
	this->reset_stage();
}

//0 = tie, 1 = p1 won, 2 = p2 won
uint8_t RPS::p1_won_round(RPS_t p1_choice, RPS_t p2_choice)
{
	switch(p1_choice)
	{
		case ROCK:
			switch(p2_choice)
			{
				case ROCK:
					return 0;
					break;
				case SCISSORS:
					return 1;
					break;
				case PAPER:
					return 2;
					break;
			}
			break;
		case SCISSORS:
			switch(p2_choice)
			{
				case ROCK:
					return 2;
					break;
				case SCISSORS:
					return 0;
					break;
				case PAPER:
					return 1;
					break;
			}
			break;
		case PAPER:
			switch(p2_choice)
			{
				case ROCK:
					return 1;
					break;
				case SCISSORS:
					return 2;
					break;
				case PAPER:
					return 0;
					break;
			}
			break;
	}
	return -1;
}
