/*
 * game_control.cpp
 *
 * This function handles the logic for the
 * Rock, Paper, Scissors game.
 *
 *  Created on: Apr 24, 2018
 *      Author: jskow
 */

//#include "game.hpp"
#include "rock_paper_scissors.hpp"
//#include "controller.hpp"
//#include "player.hpp"
//#include "display.hpp"
#include "LabGPIO.hpp"
#include "../Display/LEDMatrixGPIO.hpp"
#include "lpc_rit.h"
#include "uart0_min.h"
#include "io.hpp"
#include "../LabGPIOInterrupts.hpp"

//Signal when push button is pressed
SemaphoreHandle_t xSemaphore;
SemaphoreHandle_t xSemaphore2;
#define PORT_0 0
#define PORT_2 1

//Display referesh function
static void refresh()
{
    matrixGPIO.updateDisplay();
}

void switchIRQhdlr(void *game_in)
{
	Game *cur_game = (Game *)game_in;
	static uint32_t start_intr = 0, stop_intr = 0, mode_intr = 0, rst_intr = 0;
	static uint32_t delay = 20;
	while(1)
	{
		if((SW.getSwitch(1) == 1) && (delay == 0))
		{
			uart0_puts("start game");
			cur_game->toggle_start_intr();
			delay = 20;
			//return;
		}
		//if(SW.getSwitch(2) != stop_intr)
		//{
		//	cur_game->toggle_stop_intr();
		//}
		if((SW.getSwitch(3) == 1) && (delay == 0))
		{
			uart0_puts("switch 3 high");
			cur_game->toggle_mode_intr();
			delay = 20;
		}
		//if(SW.getSwitch(4) != rst_intr)
		//{
		//	cur_game->toggle_reset_intr();
		//}
		if(delay != 0)
		{
			delay--;
		}
		vTaskDelay(25);

	}
}

void startIRQhdlr(void *game_in)
{
	Game *cur_game = (Game *)game_in;
	while(1)
	{
		if(xSemaphoreTake(xSemaphore, portMAX_DELAY))
		{
			cur_game->toggle_start_intr();
			//uart0_puts("start toggle");
		}
		vTaskDelay(35);
	}
}

void modeIRQhdlr(void *game_in)
{
	Game *cur_game = (Game *)game_in;
	while(1)
	{
		if(xSemaphoreTake(xSemaphore2, portMAX_DELAY))
		{
			cur_game->toggle_mode_intr();
			//uart0_puts("mode toggle");
		}
		vTaskDelay(35);
	}
}

void start_isr()
{
    xSemaphoreGiveFromISR(xSemaphore, 0);
}

void mode_isr()
{
    xSemaphoreGiveFromISR(xSemaphore2, 0);
}

Controller controllers[2];
Player players[2];
bool onePlayer = true;
//TODO: Add logic to choose the game
//TODO: Add interrupts to increment mode/detect start/reset
int main()
{
	const uint32_t STACK_SIZE = 1024;
    xSemaphore = xSemaphoreCreateBinary();
    xSemaphore2 = xSemaphoreCreateBinary();

	Controller *glove_one = &controllers[0];
	Controller *glove_two = &controllers[1];
	Player *p1 = &players[0];
	Player *p2 = &players[1];
	p1->attach_contr(glove_one);
	p2->attach_contr(glove_two);

    matrixGPIO.init(); //init all pins and their default states
    rit_enable(refresh, 1/(50 * 10e6)); //repetitive timer interrupt to refresh display at 50 MHz frequency

	//RPS game_inst;
    game.init();
	game.add_player(p1, Game::PLAYER_1);
	game.add_player(p2, Game::PLAYER_2);
	//game.add_display(&matrixGPIO);

    //Get singleton instance
    LabGPIOInterrupts *switch_one = LabGPIOInterrupts::getInstance();
    LabGPIOInterrupts *switch_two = LabGPIOInterrupts::getInstance();

    //Attach interrupt handler for Start and mode switches
    switch_one->attachInterruptHandler(PORT_2, 0, start_isr, LabGPIOInterrupts::FALLING_IRQ);
    switch_one->init();
    switch_two->attachInterruptHandler(PORT_2, 1, mode_isr, LabGPIOInterrupts::FALLING_IRQ);
    switch_two->init();

	//TODO: Use interrupts instead of a polling task
	//xTaskCreate(switchIRQhdlr, "Interrupt handler", STACK_SIZE, (void *)&game, 1, NULL);
	xTaskCreate(startIRQhdlr, "Interrupt handler", STACK_SIZE, (void *)&game, 1, NULL);
	xTaskCreate(modeIRQhdlr, "Interrupt handler", STACK_SIZE, (void *)&game, 1, NULL);
	//Initialize the controller
	//These will spawn the BT listening threads/intra board receive
	glove_one->init(3);
	glove_two->init(2);

	/*
	 * In order to begin play, the user needs to:
	 * 1. Select the mode (1P vs 2P)
	 * 2. Press the start button
	 */
	//Launch P1 BT receiver task
	//If user selects P2 BT receiver task

	//Start all game related tasks
	game.start_game();


	vTaskStartScheduler();
	while(1)
	{

	}

}

