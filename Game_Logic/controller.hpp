/*
 * controller.hpp
 *
 *  Created on: Apr 28, 2018
 *      Author: jskow
 */

#ifndef L5_APPLICATION_PROJECT_LOGIC_CONTROLLER_HPP_
#define L5_APPLICATION_PROJECT_LOGIC_CONTROLLER_HPP_

#include "FreeRTOS.h"
#include "task.h"
#include "uart_dev.hpp"
#include <stdio.h>

class Controller {
private:
	//Controller is constantly sending a value of 0,1,2
	uint8_t input;
	uint8_t contr_inst;

public:
	uint8_t uart;
	TaskHandle_t handle;
	friend void uart_task(void * p);
	void init(uint8_t ut);

	//Get controller input value
	uint8_t get_input(){return this->input;};
	void set_input(uint8_t value){this->input = value;};

	//Send feedback to controller
	//Vibration motor in our case
	void send_feedback();

};



#endif /* L5_APPLICATION_PROJECT_LOGIC_CONTROLLER_HPP_ */
