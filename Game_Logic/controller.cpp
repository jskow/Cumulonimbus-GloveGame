#include "controller.hpp"
#include "uart2.hpp"
#include "uart3.hpp"
#include <stdio.h>
#include "uart0_min.h"
#define STACK_SIZE 1024

void uart_task(void *ptr)
{
	uart0_puts("uart running");
    char ch[2] = {0};
    Controller *ctrl = (Controller *)ptr;
    //printf("\nctrl.uart:%d, %p\n",ctrl.uart, &ctrl.uart);
   // printf("\nctrl.uart:%d, %p\n",ctrl.uart, &ctrl.uart);
    while(1)
    {
        if (ctrl->uart == 2)
        {
            Uart2 u2 = Uart2::getInstance();
            u2.getChar(ch, portMAX_DELAY);
            printf("u2: %c\n", ch);
        }
        else
        {
            Uart3 u3 = Uart3::getInstance();
            u3.getChar(ch, portMAX_DELAY);
            uart0_puts("uart3");
        }
        ch[1] = '\0';
        printf("Recieved %c\n", ch[0]);
        if (ch[0] == 'R')
        {
            ctrl->set_input(0);
        }
        else if (ch[0] == 'P') {
            ctrl->set_input(1);
        }
        else if (ch[0] == 'S') {
            ctrl->set_input(2);
        }
        vTaskDelay(10);
    }
}

void Controller::init(uint8_t ut)
{
    void *ptr = this;
    this->uart = ut;
    if (ut == 3)
    {
        Uart3 &u3 = Uart3::getInstance();
        u3.init(9600);
        xTaskCreate(uart_task, "Uart_Task_3", STACK_SIZE, ptr, PRIORITY_HIGH, &this->handle);
    }
    else
    {
        Uart2 &u2 = Uart2::getInstance();
        u2.init(9600);
        xTaskCreate(uart_task, "Uart_Task_2", STACK_SIZE, ptr, PRIORITY_HIGH, &this->handle);
    }

    //printf("ut: %d, %p\n", this->uart, &this->uart);
}

//Send feedback to controller
//Vibration motor in our case
void Controller::send_feedback()
{
	uart0_puts("ctrl fdbk");
    if (this->uart == 2)
    {
        Uart2 &u2 = Uart2::getInstance();
        u2.putChar('A');
    }
    else
    {
        Uart3 &u3 = Uart3::getInstance();
        u3.putChar('A');
    }
}
