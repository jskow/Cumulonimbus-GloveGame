/*
 * game.cpp
 *
 *  Created on: Apr 28, 2018
 *      Author: jskow
 */

#include "game.hpp"
#include "stdio.h"
#include "stdlib.h"
#include "uart0_min.h"
#include "tasks.hpp"
#include "io.hpp"

//Constructors
//Game::Game()
//{
//	this->start = 0;
//	this->stop = 0;
//	this->stage=0;
//	this->mode=0;
//	this->in_progress=0;
//	this->player_one_inst=nullptr;
//	this->player_two_inst=nullptr;
//	//this->display_inst=nullptr;
//	this->start_interrupt=0;
//	this->stop_interrupt=0;
//	this->reset_interrupt=0;
//	this->mode_interrupt=0;
//}

void Game::init()
{
	this->start = 0;
	this->stop = 0;
	this->stage=0;
	this->mode=0;
	this->in_progress=0;
	//this->display_inst=nullptr;
	this->start_interrupt=0;
	this->stop_interrupt=0;
	this->reset_interrupt=0;
	this->mode_interrupt=0;
}

void Game::start_game()
{
	//Create a task for the game, then start all tasks
	const uint32_t STACK_SIZE = 4096;

    //Make task handler and parameter null, as neither is needed
    xTaskCreate(run_game, "GameTask", STACK_SIZE, (void *)this, 1, NULL);
	this->start = 1; this->stop = 0; this->in_progress=1;
	this->stage = 0;

	uart0_puts("start!");
	//vTaskStartScheduler();
}

//This function relies on Games to have public functions stage_0, stage_1, etc.
void run_game(void *in_game)
{
	Game *cur_game = (Game *)in_game;
	while(1)
	{
		//Each stage should be able to be re-entered
		//Stage should save its own state in some way
		//uart0_puts("enter stage");
		//cur_game->call_stage(cur_game->get_stage());
		//cur_game->call_stage(0);
		game.call_stage(game.get_stage());
		//uart0_puts("stghdlr");
		//Handle interrupts
		//if(cur_game->mode_interrupt) cur_game->next_mode();
		//if(cur_game->stop_interrupt) cur_game->stop_game();
		//if(cur_game->start_interrupt) cur_game->start_game();
		//if(cur_game->reset_interrupt) cur_game->reset_game();
		//vTaskDelay(5);
	}
}

void Game::add_player(Player *player_inst, Game::player_num_t player_num)
{
	if(player_num == Game::PLAYER_1)
	{
		this->player_one_inst = player_inst;
	} else {
		this->player_two_inst = player_inst;
	}
}
