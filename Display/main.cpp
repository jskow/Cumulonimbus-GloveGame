/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief This is the application entry point.
 * 			FreeRTOS and stdio printf is pre-configured to use uart0_min.h before main() enters.
 * 			@see L0_LowLevel/lpc_sys.h if you wish to override printf/scanf functions.
 *
 */
#include "tasks.hpp"
#include "examples/examples.hpp"
#include "io.hpp"
#include <stdio.h>
#include "LPC17xx.h"
#include "gpio.hpp"
#include "storage.hpp"
#include "string.h"
#include "time.h"
#include "lpc_rit.h"
//#include "LEDMatrix.hpp"
#include "LEDMatrixGPIO.hpp"
#include "eint.h"
#include "semphr.h"
#include "math.h"

//globals that will be used for state machine, set default states
uint8_t curRound = 1, leftScore = 0, rightScore = 0;
#if 0
bool onePlayer = true;

static void refresh()
{
    matrixGPIO.updateDisplay();
}

//Notes: power line opposite side of data lines  to reduce flickering
void matrixGPIOTask(void *p)
{
    matrixGPIO.init(); //init all pins and their default states
    rit_enable(refresh, 1/(50 * 10e6)); //repetitive timer interrupt to refresh display at 50 MHz frequency

    matrixGPIO.drawStartScreen(); //at startup, display start screen frame and idle animation
    matrixGPIO.clearScreen();

    for( ; curRound < 4; curRound++)
    {
        matrixGPIO.drawScoreboard(curRound, leftScore, rightScore);
        //matrixGPIO.drawAction((Action) (rand() % 2), (Action) (rand() % 2));
        rightScore++;
       // matrixGPIO.drawScore(leftScore, rightScore);
        //matrixGPIO.drawRoundEnd(Player2);
    }

   // matrixGPIO.drawGameOver(Player2);

    while(1){}
}


int main(void)
{

    const uint32_t STACK_SIZE = 2048;
    xTaskCreate(matrixGPIOTask, "matrixGPIOTask", STACK_SIZE, 0, 64, NULL);


    //scheduler_add_task(new terminalTask(PRIORITY_HIGH));
    /* Start Scheduler - This will not return, and your tasks will start to run their while(1) loop */
     vTaskStartScheduler();
    /* Change "#if 0" to "#if 1" to run period tasks; @see period_callbacks.cpp */
    #if 0
    scheduler_add_task(new periodicSchedulerTask());
    #endif


    /* The task for the IR receiver */
    // scheduler_add_task(new remoteTask  (PRIORITY_LOW));

    /* Your tasks should probably used PRIORITY_MEDIUM or PRIORITY_LOW because you want the terminal
     * task to always be responsive so you can poke around in case something goes wrong.
     */

    /**
     * This is a the board demonstration task that can be used to test the board.
     * This also shows you how to send a wireless packets to other boards.
     */
    #if 0
        scheduler_add_task(new example_io_demo());
    #endif

    /**
     * Change "#if 0" to "#if 1" to enable examples.
     * Try these examples one at a time.
     */
    #if 0
        scheduler_add_task(new example_task());
        scheduler_add_task(new example_alarm());
        scheduler_add_task(new example_logger_qset());
        scheduler_add_task(new example_nv_vars());

    #endif

    /**
	 * Try the rx / tx tasks together to see how they queue data to each other.
	 */
    #if 0
        scheduler_add_task(new queue_tx());
        scheduler_add_task(new queue_rx());
    #endif

    /**
     * Another example of shared handles and producer/consumer using a queue.
     * In this example, producer will produce as fast as the consumer can consume.
     */
    #if 0
        scheduler_add_task(new producer());
        scheduler_add_task(new consumer());
    #endif

    /**
     * If you have RN-XV on your board, you can connect to Wifi using this task.
     * This does two things for us:
     *   1.  The task allows us to perform HTTP web requests (@see wifiTask)
     *   2.  Terminal task can accept commands from TCP/IP through Wifly module.
     *
     * To add terminal command channel, add this at terminal.cpp :: taskEntry() function:
     * @code
     *     // Assuming Wifly is on Uart3
     *     addCommandChannel(Uart3::getInstance(), false);
     * @endcode
     */
    #if 0
        Uart3 &u3 = Uart3::getInstance();
        u3.init(WIFI_BAUD_RATE, WIFI_RXQ_SIZE, WIFI_TXQ_SIZE);
        scheduler_add_task(new wifiTask(Uart3::getInstance(), PRIORITY_LOW));
    #endif

    //scheduler_start(); ///< This shouldn't return
    return -1;
}
#endif


