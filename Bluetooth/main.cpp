/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief This is the application entry point.
 * 			FreeRTOS and stdio printf is pre-configured to use uart0_min.h before main() enters.
 * 			@see L0_LowLevel/lpc_sys.h if you wish to override printf/scanf functions.
 *
 */
// #include "LABGPIO.hpp"

#include <stdio.h>
// #include "utilities.h"
#include "io.hpp"
#include "uart2.hpp"
#include "uart3.hpp"
#include "tasks.hpp"

#define STACK_SIZE 1024

#define DEBUG 1

void    vibrateTask(void * p)
{
    Uart2 &u2 = Uart2::getInstance();
    Uart3 &u3 = Uart3::getInstance();
    char ch[2] = {0};

    while(1)
    {
        if(u3.getChar(ch, 0))
        {
            ch[1] = '\0';
            u2.putChar(ch[0]);
        }
        vTaskDelay(1);
    }
}

void  relayTask(void * p)
{
    char ch[2] = {0};
    Uart2 &u2 = Uart2::getInstance();
    Uart3 &u3 = Uart3::getInstance();
    while(1)
    {
        if(u2.getChar(ch, 0))
        {
            ch[1] = '\0';
            u3.putChar(ch[0]);
#if DEBUG
            if (ch[0] == 'R')
            {
                printf("rock recieved\n");
            }
            else if (ch[0] == 'P')
            {
                printf("paper recieved\n");
            }
            else if (ch[0] == 'S')
            {
                printf("scissors recieved\n");
            }
#endif
        }
        vTaskDelay(1);
    }
}
#if 0
int main (void)
{
    Uart2 &u2 = Uart2::getInstance();
    Uart3 &u3 = Uart3::getInstance();
    u2.init(9600);
    u3.init(9600);
    xTaskCreate(relayTask, "RelayTask", STACK_SIZE, (void *)'a', PRIORITY_MEDIUM, NULL);
    xTaskCreate(vibrateTask, "vibrateTask", STACK_SIZE, (void *)'a', PRIORITY_LOW, NULL);
    vTaskStartScheduler();
    return 0;
}
#endif